										  //ALBERO BINARIO DI RICERCA

#include<stdio.h>
#include<stdlib.h>
struct node
{
    int data;
    int livello;
    struct node* left;
    struct node* right;
};

struct node* createNode(int value, int clivello) {
    struct node* newNode =(node *) malloc(sizeof(struct node));//	allocare spazio per il nodo
    newNode->data = value;
    newNode->livello=clivello;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
}

struct node* insert(struct node* root, int data, int &clivello)
{
    if (root == NULL) 
		return createNode(data, clivello);
    if (data < root->data){
    	clivello++;
    	root->left  = insert(root->left, data, clivello);
    }
    else if (data > root->data){
    	clivello++;
        root->right = insert(root->right, data, clivello);   
	}
    return root;
}

void inorder(struct node* root) {
    if(root == NULL) return;
    inorder(root->left);
    printf("%d ->", root->data);
    inorder(root->right);
}

bool Search(struct node* root, int number){
	if (root == NULL)
		return NULL;
	else if (number == root->data){
		printf("\nil numero cercato si trova nel livello numero ( %d )\n",root->livello);
		return true;
	}
	else if (number < root->data){
		return Search(root->left, number);
	}
	else if (number > root->data){
		return Search(root->right, number);
	}
}

void contaFoglieNodi(struct node* root, int &cf, int &cn, float &sommaf, float &somman){
	if(root->left==NULL && root->right==NULL){
		cf++;
		sommaf=sommaf+root->data;
	}
	else{
		cn++;
		somman=somman+root->data;
	}
	if(root->left!=NULL){
		contaFoglieNodi(root->left, cf, cn, sommaf, somman);
	}
	if(root->right!=NULL){
		contaFoglieNodi(root->right, cf, cn, sommaf, somman);
	}
}

float Fmedia(float somma, int c){
	float media=0;
	media= somma / c;
	return media;
}

int main(){
	int numero=0, livello=0, scelta=0, dim=0, cf=0, cn=0;
	float sommaf=0 ,somman=0, media=0;
	struct node *root = NULL;
	printf("inserire la radice dell' albero: ");
	scanf("%d", &numero);
	root = insert(root, numero, livello);
	do{	
		printf("\n\n\n\tMENU\n1-inserire altri numeri.\n2-visualizzare l'albero.\n3-cercare un numero.\n4-informazioni sulle foglie.\n5-informazioni sui nodi.\n6-esci.\n");
		printf("inserire la scelta: "); scelta=0;
		scanf("%d", &scelta);
		system("cls");
		sommaf=NULL ,somman=NULL, media=NULL, cf=NULL, cn=NULL;
		contaFoglieNodi(root, cf, cn, sommaf, somman);
		
		if(scelta==1){
			printf("inserire la quantita di numeri: ");
			scanf("%d", &dim);
			for(int i=0;i<dim;i++){
				livello=0;
				printf("inserire il numero(%d): ", i+1);
				scanf("%d", &numero);
				insert(root, numero, livello);
			}
		}
	
		else if(scelta==2){inorder(root);}
	
		else if(scelta==3){
			printf("inserire il numero da cercare: ");
			scanf("%d", &numero);
			if(Search(root, numero) ==false){printf("\nnumero non trovato.");}
		}
			
		else if(scelta==4){
			media=Fmedia(sommaf, cf);
		   	printf("\n\n\t(Foglie)\n");
		   	printf("numero: %d\nla somma: %f\nmedia: %f\n", cf, sommaf, media);
		}
		
		else if(scelta==5){
			media=Fmedia(sommaf, cn);
		   	printf("\n\t(Nodi)\n");
		   	printf("il numero: %d\nla somma: %f\nla media: %f\n", cn, somman, media);
		}
	}while(scelta!=6);
}
